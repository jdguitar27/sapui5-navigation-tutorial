sap.ui.define([
    "sap/ui/demo/nav/controller/BaseController",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
    "sap/ui/model/Sorter",
    "sap/m/ViewSettingsDialog",
    "sap/m/ViewSettingsItem"
], function(BaseController, Filter, FilterOperator, Sorter, ViewSettingsDialog, ViewSettingsItem) {
    'use strict';
    
    return BaseController.extend("sap.ui.demo.nav.controller.employee.overview.EmployeeOverviewContent", {

        onInit: function () {
            this._oTable = this.byId("employeesTable");
            this._oVSD = null;
            this._sSortField = null;
            this._bSortDescending = false;
            this._aValidSortFields = ["EmployeeID", "FirstName", "LastName"];
            this._sSearchQuery = null;

            this._initViewSettingsDialog();
        },

        onSortButtonPressed: function () {
            this._oVSD.open();
        },

        onSearchEmployeesTable: function (oEvent) {
            this._applySearchFilter( oEvent.getSource().getValue() );
        },

        _initViewSettingsDialog: function () {
            this._oVSD = new ViewSettingsDialog("vsd", {
                confirm: function (oEvent) {
                    var oSortItem = oEvent.getParameter("sortItem");
                    this._applySorter(oSortItem.getKey(), oEvent.getParameter("sortDescending"));
                }.bind(this)
            });
        }
    });
});