from flask import Flask, request, jsonify

app = Flask(__name__)

@app.route('/employees', methods=['GET'])
def get_products():
    products = [
        {
        'id': 1,
        'name': "product 1",
        'description': 'Eiusmod ad qui cupidatat enim nulla laborum sunt fugiat aute labore.'
        },
        {
            'id': 2,
            'name': "product 2",
            'description': 'Aliquip aliqua eiusmod enim excepteur tempor sint occaecat veniam elit.'
        },
        {
            'id': 3,
            'name': "product 3",
            'description': 'Aliqua id esse nostrud ipsum consectetur in ad sunt.'
        },
        {
            'id': 4,
            'name': "product 4",
            'description': 'Esse tempor dolor nostrud incididunt incididunt ipsum laborum id. In irure minim culpa labore duis dolore cupidatat et proident qui do excepteur. Cillum eiusmod est aute reprehenderit est et dolore consectetur dolor esse mollit. Enim proident sunt excepteur cillum pariatur velit tempor velit excepteur culpa deserunt. Cupidatat ipsum labore voluptate fugiat eu officia voluptate veniam esse adipisicing occaecat tempor anim. Incididunt eiusmod exercitation veniam exercitation laboris quis culpa consequat reprehenderit minim anim minim qui nostrud. Nisi excepteur Lorem qui dolor magna labore pariatur dolore voluptate.'
        },
        {
            'id': 5,
            'name': "product 5",
            'description': 'Minim proident eiusmod aliqua laborum id ea cillum excepteur do velit esse non ut ea. Dolor officia qui nostrud eu velit consequat consectetur eiusmod ex. Officia deserunt pariatur duis ipsum deserunt. Aliquip do enim aliquip proident consequat.'
        },
        {
            'id': 6,
            'name': "product 6",
            'description': 'Aliquip aliqua eiusmod enim excepteur tempor sint occaecat veniam elit.'
        }
    ]
    return jsonify(products=products)

if __name__ == '__main__':
    app.run(port=5000)